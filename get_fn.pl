#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use utf8;

$LWP::Simple::ua->default_header('Referer' => "https://www.candidats.fr");
$LWP::Simple::ua->agent('Mozilla/5.0');

$|=1;
my $page = get('https://fnlegislatives.fr/vos-candidats/');

LISTE:
foreach my $ligne (split /></, $page)
{
  my $url;
  if ($ligne =~ /a class="tg-link-button" href="(https:\/\/fnlegislatives.fr\/candidats\/.+?\/)" target="_self/)
  {
    my $pagec = get($1);
	my ($departement, $circonscription, $candidat);
	my $site ='';
	my $fixe = '';
	my $mobile = '';
	my $email = '';
	my $current_key = '';
    foreach my $lignec (split /\n/, $pagec)
    {
      if ($lignec =~ /class="tax-item-category">([0-9AB]+)/)
      {
        $departement = $1;
      }
      elsif ($lignec =~ /class="tax-item-category">Fran.+?</)
      {
        $departement = 'ZZ';
      }
      elsif ($lignec =~ /class="tax-item-location">(\d+)/)
      {
        $circonscription = $1;
      }
      elsif ($lignec =~ /class="jv-listing-title">(.+?)</)
      {
        $candidat = $1;
      }
      elsif ($lignec =~ /<span>(Email|Fixe|Mobile|Site)<\/span>/)
      {
        $current_key = $1;
      }
	  elsif ($current_key eq 'Site' && $lignec =~ /href="(http.+?)"/)
      {
        $site = $1;
        $current_key = '';
      }
	  elsif ($current_key eq 'Email' && $lignec =~ /<span>(.+\@.+)<\/span>/i)
      {
        $email = $1;
        $current_key = '';
      }
	  elsif ($current_key eq 'Fixe' && $lignec =~ /tel:\/\/([0-9 ]+)/)
      {
        $fixe = $1;
        $current_key = ''
      }
	  elsif ($current_key eq 'Mobile' && $lignec =~ /tel:\/\/([0-9 ]+)/)
      {
        my $mobile = $1;
        
        if ($fixe)
        {
          if ($mobile)
          {
            print "$departement;$circonscription;FN;$candidat;$fixe, $mobile;$email;$site;\n";
          }
          else
          {
            print "$departement;$circonscription;FN;$candidat;$fixe, $mobile;$email;$site;\n";
          }
        }
        else
        {
          print "$departement;$circonscription;FN;$candidat;$mobile;$email;$site;\n";
        }
      }
    }
  }
}

