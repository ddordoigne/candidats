#!/bin/bash

wget http://www.nosdonnees.fr/wiki/images/e/ed/Circonscriptions.csv
cut -f 2,3 Circonscriptions.csv --output-delimiter=+ |perl -pe 's/^(.+?)\+(.+)$/\1circo\2\@en-marche.fr/' > circb
cut -f 2,3 Circonscriptions.csv --output-delimiter=+ |grep -v '..+' |perl -pe 's/^(.+?)\+(.+)$/0\1circo\2\@en-marche.fr/' >> circb
cut -f 2,3 Circonscriptions.csv --output-delimiter=+ |grep -v '..+' |perl -pe 's/^(.+?)\+(.+)$/0\1circo0\2\@en-marche.fr/' >> circb
cut -f 2,3 Circonscriptions.csv --output-delimiter=+ |grep -v '..+' |perl -pe 's/^(.+?)\+(.+)$/\1circo0\2\@en-marche.fr/' >> circb
cut -f 2,3 Circonscriptions.csv --output-delimiter=+ |grep '..+.$' |perl -pe 's/^(.+?)\+(.+)$/\1circo0\2\@en-marche.fr/' >> circb 
for mel in $(cat circb); do curl -s https://tontonroger.org/?q=$mel  |grep "result.*$mel" >/dev/null && echo $mel; done

