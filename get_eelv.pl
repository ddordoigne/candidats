#!/usr/bin/perl

use strict;
use warnings;
use LWP::UserAgent;
use utf8;

my $ua = LWP::UserAgent->new(
   agent => 'Mozilla/5.0',
   default_header => ( 'Referer' => 'https://www.candidats.fr')
);

$|=1;

my $page = $ua->get('https://git.eelv.fr/eelv/open-data/raw/master/Legislatives2017/candidat-es_EELV_au_23_mai_2017-legislatives.csv');
LISTE:
foreach my $ligne (split /\n/, $page->decoded_content)
{
  chomp $ligne;
  
  my ($region, $dpt, $circ, $pnom, $nom) = $ligne =~ /^.*?,(.+?),(..)-(\d\d),(.+?),(.+?)\W*$/ or next LISTE;
  $dpt = 974 if ($region eq 'REUNION');

  my $nom_url = "$pnom-$nom";
  $nom_url =~ s/œ/oe/g;
  $nom_url =~ tr/A-ZÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ /a-zaaaaaaceeeeiiiinooooouuuuyaaaaaaceeeeiiiinooooouuuuyy\-/;
  $nom_url =~ tr/a-z\-//cd;

  my $web = "http://aveclecologie.fr/candidat-e/$nom_url/";
  
  my $res = $ua->get($web);

  next unless ($res->{'_rc'} < 400);

  my $search_facebook = 1;
  my $search_twitter = 1;
  my $search_insta = 0;
  my $search_web = 0;
  my @resal = ();
  
  foreach my $info (split(/></, $res->content))
  {
    if ($search_twitter)
    {
      if ($info =~ /href="(https:\/\/twitter.com.+?)\s*"/)
      {
        push(@resal, $1) unless ($1 =~ /\/EELV\//);
        $search_twitter = 0;
      }
      elsif ($info =~ /href="\@(.+?)\s*"/)
      {
        push(@resal, "https://twitter.com/$1");
        $search_twitter = 0;
      }
    }
    
    if ($search_facebook && $info =~ /href="(https:\/\/(?:www.)?facebook.com\/.+?)(?:\/.*)?\s*"/)
    {
      push(@resal, $1) unless ($1 =~ /\/Europe-Écologie-Les-Verts/);
      $search_facebook = 0;
    }
    
    if ($info =~ /wp-candidate-social-instagram/)
    {
      # LE PROCHAIN LIEN EST INSTA
      $search_insta++;
    }
    elsif ($info =~ /wp-candidate-social-website/)
    {
      # LE PROCHAIN LIEN EST web
      $search_web++;
    }
    elsif ($search_insta && $info =~ /href="(.+?)\s*"/)
    {
      push(@resal, "instagram:$1");
      $search_insta = 0;  
    }
    elsif ($search_web && $info =~ /href="(.+?)\s*"/)
    {
      $web = $1;
      $search_web = 0;  
    }
  }
  print "$dpt;$circ;EELV;$pnom $nom;;;$web;".join(' ', @resal)."\n";    
}


