#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use utf8;

$LWP::Simple::ua->default_header('Referer' => "https://www.candidats.fr");
$LWP::Simple::ua->agent('Mozilla/5.0');
$|=1;

my $page = get('https://legislatives.upr.fr');

my ($departement, $circonscription, $nom);

foreach my $ligne (split /\n/, $page)
{
  chomp $ligne;

  if ($ligne =~ /^<tr id="depD(.+?)_C(\d+)_A">/)
  {
    $departement = $1;
    $circonscription = $2;
  }
  elsif (defined($circonscription))
  {
    if ($ligne =~ /^<td><span style="font-weight:bold;font-size:1.2em">(.+?)</)
    {
      $nom = $1;
    }
    elsif ($ligne =~ /^<td><a href="mailto:(.+?)"/)
    {
      my $email = $1;
      my @resal = $ligne =~ /^<td><a href="mailto:.+?"(?:.+?)href="(.+?)"/;
      print "$departement;$circonscription;UPR;$nom;;$email;;".join(' ', @resal)."\n";    
      undef($circonscription);
    }
  }
}

