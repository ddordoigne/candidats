#!/usr/bin/perl

use strict;
use warnings;
use HTTP::Cookies;
use LWP::UserAgent;
$|=1;
# format du csv (seuls les deux premiers champs ne sont pas facultatifs) : 
# departement;circonscription;candidat

# arguments : login, password, csv_entrant, csv_sortant
my $login = shift;
my $password = shift;
my $csv_entrant = shift;
my $csv_sortant = shift;

my $cookie_jar = HTTP::Cookies->new(
   file     => '/tmp/candidats.fr_cookie',
   autosave => 1,
);

my $ua = LWP::UserAgent->new(
   agent => 'https://framagit.org/ddordoigne/candidats',
   cookie_jar => $cookie_jar
);

# connexion
$ua->post('https://campagnes.candidats.fr/legislatives2017/index.php', ['action' => 'do_login', 'login' =>  $login, 'passwd' => $password] );

# si pas d'argument, on lit dans stdin et ecrit dans stdout
if (defined($csv_sortant))
{ 
  open (IN, $csv_entrant) or die $!;
  open (OUT, ">$csv_sortant") or die $!;
}
else
{
  open IN, '-';
  open OUT, '>-' or die $!;
}
while (my $candidat = <IN>)
{
  chomp $candidat;
  my ($dpt, $circ, $nom_pref) = split(';', $candidat);
  
  # recuperation de l'id de circonscription
  my $id_coll = get_coll($dpt);
  my $circos = $ua->get("https://campagnes.candidats.fr/legislatives2017/?action=liste_circonscriptions&collectivite=$id_coll");
  my ($id_circ) = $circos->as_string =~ /circonscription=(\d+)">$circ/s;

  
  # recuperation de l'id de candidat
  my $id_cand='candidat non trouve';
  my $nom='aucun';
  my $high_score = -1;

  my $cands = $ua->get("https://campagnes.candidats.fr/legislatives2017/?action=liste_candidats&circonscription=$id_circ");
  my %candidats_trouves = $cands->decoded_content =~ /action=editer_candidat\&amp;id=(\d+)">(.+?)</sg;
  
  foreach my $c (keys %candidats_trouves)
  {
    $candidats_trouves{$c} =~ s/\s+/ /gs;
    
    my $score = ressemblance($nom_pref, $candidats_trouves{$c});
    
    if ($score > $high_score)
    {
      $id_cand = $c;
      $nom = $candidats_trouves{$c};
      $high_score = $score;
    }    
  }

    
  print OUT "$dpt;$circ;$nom_pref;$nom;$id_cand;https://campagnes.candidats.fr/legislatives2017/?action=editer_candidat&id=$id_cand\n";    
}
close IN;
close OUT;

# fermeture de session
$ua->get('https://campagnes.candidats.fr/legislatives2017/?action=do_logout');

# recupere l'id de collectivite
sub get_coll
{
  my $dpt = shift;
  my %coll = ('2A' => 20, '2B' => 21, '971' => 97, '972' => 98, '973' => 99, '974' => 100, '976' => 102, '977' => 36745, '978' => 36753, '986' => 36752, '987' => 36754, '988' => 105, '99' => 36746);
  
  return $coll{$dpt} if defined($coll{$dpt});
  return $dpt+1 if ($dpt > 20);
  return $dpt;
}

# calcul approximatif de ressemblance de noms
sub ressemblance
{
  my $chaine1 = shift;
  my $chaine2 = shift;
  my $score = 0;

  COMPTAGE: 
  foreach my $terme1 (get_termes($chaine1))
  {
    foreach my $terme2 (get_termes($chaine2))
    {
      if ($terme1 eq $terme2)
      {
        $score++;
        next COMPTAGE;
      }
    }
  }
  return $score;
}

sub get_termes
{
  my $chaine = shift;
  $chaine =~ s/œ/oe/g;
  $chaine =~ s/æ/ae/g;
  $chaine =~ tr/A-ZÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ/a-zaaaaaaceeeeiiiinooooouuuuyaaaaaaceeeeiiiinooooouuuuyy/;
  return split(/[^a-z]/, $chaine);
}
