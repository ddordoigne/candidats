#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use utf8;

$LWP::Simple::ua->default_header('Referer' => "https://www.candidats.fr");
$LWP::Simple::ua->agent('Mozilla/5.0');
$|=1;


foreach my $departement (1..19, '2A', '2B', 21..95, 99, 971..976, 987, 988)
{
  CIRCO:
  for (my $circonscription = 1; $circonscription < 22; $circonscription++)
  {
    my $page = get("https://legislatives2017.lafranceinsoumise.fr/departement/$departement/circonscription/$circonscription");
    my ($candidat, $email) = $page =~ /^.+?<h4>(.+?)<\/h4>.+?"mailto:(.*?)"/s or next CIRCO;
    print "$departement;$circonscription;FI;$candidat;;$email;;\n" if ($email);
  }
}

