#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use utf8;

$LWP::Simple::ua->default_header('Referer' => "https://www.candidats.fr");
$LWP::Simple::ua->agent('Mozilla/5.0');
$|=1;

my $page = get('http://elections.interieur.gouv.fr/legislatives-2017/index.html');

DPT:
foreach my $ligne (split /\n/, $page)
{
 
  if ($ligne =~ /^<option value="(0*([0-9AB]+)\/index\.html)"/)
  {
    my $paged = get("http://elections.interieur.gouv.fr/legislatives-2017/$1");
    my $departement = $2;
    
    CIRC:
    foreach my $ligned (split /circonscription/, $paged)
    {
      next CIRC unless $ligned =~ /"..\/(0*$departement\/0*$departement\d+\.html)">(\d+)<sup>/;
      my $circo = $2;
      my $pagec = get("http://elections.interieur.gouv.fr/legislatives-2017/$1"); 

      my @candidats = split(/<tr>/, $pagec);
  
      foreach my $c (@candidats)
      {
        if ($c =~ /<td style="text-align:left">M.+?\s+(.+?)<\/td>\n<td style="text-align:center">.+?<\/td>\n<td style="text-align:right">.+?<\/td>\n<td style="text-align:center">.+?<\/td>\n<td style="text-align:center">.+?<\/td>\n<td style="text-align:center">[^N]/s)
        {
          print "$departement;$circo;$1\n";
        }
      }
    }
  }
}

