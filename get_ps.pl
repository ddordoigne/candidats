#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use utf8;

$LWP::Simple::ua->default_header('Referer' => "https://www.candidats.fr");
$LWP::Simple::ua->agent('Mozilla/5.0');
$|=1;

my $page = get('https://www.parti-socialiste.fr/liste-candidats-aux-legislatives-investis-ps/');

my $departement = 0;
my $circonscription = 0;
LISTE:
foreach my $ligne (split /\n/, $page)
{
  my $url;
  if ($ligne =~ /^<li>.+? \((\d+)\)<\/span>/)
  {
    $departement = $1;
  }
  elsif ($ligne =~ /Département ([0-9AB]+)/)
  {
    $departement = $1;
  }

  if ($ligne =~ /(\d+)..e circonscription.+?href="(.+?)"/)
  {
    $circonscription = $1;
    $url=$2;
    my $candidat;
    my $facebook = '';
    my $twitter = '';
  
    my $pagec = get($url);

    foreach my $lignec (split /\n/, $pagec)
    {
      if ($lignec =~ /^\s+<h1 class="article-title">(.+?)<\/h1>/)
      {
         $candidat = $1;
      }
      if (defined($candidat))
      {
         if ($lignec =~ /href="(https:\/\/twitter.com\/.+?)\/?\?lang=/)
         {
           $twitter = $1;
         }
         elsif ($lignec =~ /href="(https:\/\/twitter.com\/.+?)"/)
         {
           $twitter = $1;
         }
         elsif ($lignec =~ /href="(https:\/\/(?:www.)?facebook.com\/.+?)(?:\/.*)?"/)
         {
           $facebook = $1;
         }
         elsif ($lignec =~ /class="article-description"/)
         {
           if($twitter) 
           {
             if($facebook)
             {
               print "$departement;$circonscription;PS;$candidat;;;;$twitter $facebook\n";
             }
             else
             {
               print "$departement;$circonscription;PS;$candidat;;;;$twitter\n";
             }
           }
           elsif($facebook)
           {
             print "$departement;$circonscription;PS;$candidat;;;;$facebook\n";
           }
           next LISTE;
         }
      }
    }
  }
}

