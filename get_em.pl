#!/usr/bin/perl

use strict;
use warnings;
use LWP::UserAgent;
use utf8;

my $ua = LWP::UserAgent->new(
   agent => 'Mozilla/5.0',
   default_header => ( 'Referer' => 'https://www.candidats.fr')
);

$|=1;

my %departements =
(
  'AIN' => 1,
  'AISNE' => 2,
  'ALLIER' => 3,
  'ALPES DE HAUTE PROVENCE' => 4,
  'HAUTES ALPES' => 5,
  'ALPES MARITIMES' => 6,
  'ARDECHE' => 7,
  'ARDENNES' => 8,
  'ARIEGE' => 9,
  'AUBE' => 10,
  'AUDE' => 11,
  'AVEYRON' => 12,
  'BOUCHES DU RHONE' => 13,
  'CALVADOS' => 14,
  'CANTAL' => 15,
  'CHARENTE' => 16,
  'CHARENTE MARITIME' => 17,
  'CHER' => 18,
  'CORREZE' => 19,
  'CORSE DU SUD' => '2A',
  'HAUTE CORSE' => '2B',
  "COTE D'OR" => 21,
  "COTES D'ARMOR" => 22,
  'CREUSE' => 23,
  'DORDOGNE' => 24,
  'DOUBS' => 25,
  'DROME' => 26,
  'EURE' => 27,
  'EURE ET LOIR' => 28,
  'FINISTERE' => 29,
  'GARD' => 30,
  'HAUTE GARONNE' => 31,
  'GERS' => 32,
  'GIRONDE' => 33,
  'HERAULT' => 34,
  'ILLE ET VILAINE' => 35,
  'INDRE' => 36,
  'INDRE ET LOIRE' => 37,
  'ISERE' => 38,
  'JURA' => 39,
  'LANDES' => 40,
  'LOIR ET CHER' => 41,
  'LOIRE' => 42,
  'HAUTE LOIRE' => 43,
  'LOIRE ATLANTIQUE' => 44,
  'LOIRET' => 45,
  'LOT' => 46,
  'LOT ET GARONNE' => 47,
  'LOZERE' => 48,
  'MAINE ET LOIRE' => 49,
  'MANCHE' => 50,
  'MARNE' => 51,
  'HAUTE MARNE' => 52,
  'MAYENNE' => 53,
  'MEURTHE ET MOSELLE' => 54,
  'MEUSE' => 55,
  'MORBIHAN' => 56,
  'MOSELLE' => 57,
  'NIEVRE' => 58,
  'NORD' => 59,
  'OISE' => 60,
  'ORNE' => 61,
  'PAS DE CALAIS' => 62,
  'PUY DE DOME' => 63,
  'PYRENEES ATLANTIQUES' => 64,
  'HAUTES PYRENEES' => 65,
  'PYRENEES ORIENTALES' => 66,
  'BAS RHIN' => 67,
  'HAUT RHIN' => 68,
  'RHONE' => 69,
  'HAUTE SAONE' => 70,
  'SAONE ET LOIRE' => 71,
  'SARTHE' => 72,
  'SAVOIE' => 73,
  'HAUTE SAVOIE' => 74,
  'PARIS' => 75,
  'SEINE MARITIME' => 76,
  'SEINE ET MARNE' => 77,
  'YVELINES' => 78,
  'DEUX SEVRES' => 79,
  'SOMME' => 80,
  'TARN' => 81,
  'TARN ET GARONNE' => 82,
  'VAR' => 83,
  'VAUCLUSE' => 84,
  'VENDEE' => 85,
  'VIENNE' => 86,
  'HAUTE VIENNE' => 87,
  'VOSGES' => 88,
  'YONNE' => 89,
  'TERRITOIRE' => 90,
  'ESSONNE' => 91,
  'HAUTS DE SEINE' => 92,
  'SEINE SAINT DENIS' => 93,
  'VAL DE MARNE' => 94,
  "VAL D'OISE" => 95,
  'GUADELOUPE' => 971,
  'MARTINIQUE' => 972,
  'GUYANE' => 973,
  'LA REUNION' => 974,
  'SAINT-MARTIN/SAINT-BARTHELEMY' => 977,
  "FRANCAIS DE L'ETRANGER" => 99
);


my $page = $ua->get('https://en-marche.fr/article/communique-liste-investis');
LISTE:
foreach my $ligne (split /\n/, $page->as_string)
{
  my ($nom_dpt, $circo, $nom_candidat) = $ligne =~ /^<p>(\S+)\s(\d+),\s+([^\(]+?)<\/p>/ or next LISTE;
  
  my $dpt = $departements{$nom_dpt} or die $nom_dpt;
  my $nom_url = $nom_candidat;
  $nom_url =~ tr/A-Za-z /e/c;   
  $nom_url =~ tr/A-Z /a-z/d;

  my $res = $ua->get("https://$nom_url.nationbuilder.com/");

  # on ignore ceux qui n'ont pas de page
  next unless ($res->{'_rc'} < 400);
  
  # page web du candidat
  my $web = $res->{'_request'}->{'_uri'};
  
  my ($email) = $res->as_string =~ /href="mailto:(.+\@.+?)["\?]/s;

  print "$dpt;$circo;EM;$nom_candidat;;$email;$web;\n";

   sleep 1;
}


